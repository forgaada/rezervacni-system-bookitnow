# Rezervační systém BookitNow

Hlavním cílem projektu „Bookit Now“ je vytvořit adekvátní rezervační systém umožňující online rezervaci sportovišť a služeb nabízených sportovním centrem.<br>

Více na [Wiki](https://gitlab.fel.cvut.cz/forgaada/rezervacni-system-bookitnow/wikis/home)